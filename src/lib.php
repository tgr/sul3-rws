<?php

function getCurrentSite() {
    // Can't use $_SERVER['REQUEST_SCHEME'] here, Toolforge always sees http:
    return 'https://' . $_SERVER['SERVER_NAME'];
}

function assertCurrentSite( $site ) {
    if ( getCurrentSite() !== $site ) {
        echo "This URL should only be used on $site";
        die;
    }
}

function getCSPDomainList( $config ) {
    return [ $config['sites']['primary'] ] + ( $config['sites']['associated'] ?? [] );
}

function render( $template, $variables = [] ) {
    extract( $variables );
    include( "templates/$template.php" );
}