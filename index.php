<?php
// Main site entry point

$config = require_once 'config.php';
require_once 'src/lib.php';

assertCurrentSite( $config['sites']['primary'] );

$loginSite = $config['sites']['login'];
$returnto = getCurrentSite() . '/return.php';
$askForPermission = $_GET['askForPermission'] ?? false;

render( 'main', [
    'loginSite' => $loginSite,
    'returnto' => $returnto,
    'action' => $_GET['action'] ?? 'autologin',
    'method' => $_GET['method'] ?? 'popup',
    'useRequestStorageAccessFor' => $_GET['useRequestStorageAccessFor'] ?? false,
    'askForPermission' => $askForPermission,
    'topLevelUrl' => $loginSite . '/login.php?' . http_build_query( [
        'method' => 'toplevel',
        'returnto' => $returnto,
        'askForPermission' => $askForPermission,
    ] ),
] );