<?php
// Entry point on all sites for the RWS well-known URL, at .well-known/related-website-set.json


$config = require_once 'config.php';
require_once 'src/lib.php';

$json = [ 'primary' => $config['sites']['primary'] ];
if ( getCurrentSite() === $config['sites']['primary'] ) {
    $associatedSites = $config['sites']['associated'] ?? [];
    if ( $associatedSites ) {
        $json['associatedSites'] = $associatedSites;
    }
    $json['serviceSites'] = [ $config['sites']['login'] ];
    $json['rationaleBySite'] = [
        $config['sites']['login'] => 'Used for SSO',
    ] + array_fill_keys( $associatedSites, 'Affiliation indicated via footer badge' );
}

header( 'Content-Type: application/json' );
echo json_encode( $json );