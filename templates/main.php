<?php // Main HTML page
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SUL3 RWS</title>
    <link rel="stylesheet" href="css/main.css">
    <script type="application/json" id="data"><?= json_encode( [
        'loginSite' => $loginSite,
        'returnto' => $returnto,
    ] ) ?></script>
</head>
<body>
    <a href="<?= $topLevelUrl ?>" id="loginLink">Login</a>
    <div id="loginLinkTooltip" class="hidden">Click here to log in.</div>
    <form id="config-form" method="get" action="">
        Configuration for the login logic. Note that you might need to reload the page for these to take effect.
        <select id="action" name="action">
            <option value="login" <?php if ( $action === 'login' ) echo 'selected'; ?>>Login</option>
            <option value="autologin" <?php if ( $action === 'autologin' ) echo 'selected'; ?>>Auto-login only</option>
        </select>
        <select id="method" name="method">
            <option value="popup" <?php if ( $method === 'popup' ) echo 'selected'; ?>>Popup</option>
            <option value="iframe" <?php if ( $method === 'iframe' ) echo 'selected'; ?>>Iframe</option>
        </select>
        <label for="useRequestStorageAccessFor">
            <input type="checkbox" id="useRequestStorageAccessFor" name="useRequestStorageAccessFor" <?php if ( $useRequestStorageAccessFor ) echo 'checked'; ?> />
            Use requestStorageAccessFor
        </label>
        <label for="askForPermission">
            <input type="checkbox" id="askForPermission" name="askForPermission" <?php if ( $askForPermission ) echo 'checked'; ?> />
            Ask for permission
        </label>
        <button type="submit">Apply</button>
    </form>
    <div id="permissionButtonContainer" class="hidden">
        <button type="button" id="permissionButton">Click here to check login status</button>
    </div>
    <div id="iframeContainer"></div>
    <div id="result"></div>
    <footer>
        <a class="source" href="https://gitlab.wikimedia.org/tgr/sul3-rws">Source code</a>
        <a class="readme" href="https://gitlab.wikimedia.org/tgr/sul3-rws/-/blob/main/README.md">README</a>
        <a class="phab" href="https://phabricator.wikimedia.org/T359926">Phabricator task</a>
    </footer>
    <script src="js/lib.js"></script>
    <script src="js/main.js"></script>
</body>
</html>