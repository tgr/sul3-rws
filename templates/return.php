<!DOCTYPE html><html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
    <?php if ( $username ) { ?>
        <h1>Logged in as <?= $username ?></h1>
    <?php } else { ?>
        <h1>Not logged in</h1>
    <?php } ?>
</body>
</html>