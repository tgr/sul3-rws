<!DOCTYPE html><html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <script type="application/json" id="data"><?= json_encode( [
        'returnto' => $returnto,
        'username' => $username,
    ] ) ?></script>
</head>
<body>
    <script src="js/lib.js"></script>
    <script src="js/login.js"></script>
</body>
</html>