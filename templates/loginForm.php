<?php
// Client-side logic will determine which one of these to show.
// If we have a username, we show the login button, and add a hidden login form to allow the user to change identities.
// If we don't have a username, it's still possible that we can obtain it on the client side after calling
// requestStorageAccess, so we show the login form and add a hidden login button, and swap if needed.
?><!DOCTYPE html><html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
    <form id="loginButtonForm" action="<?= $returnto ?>" class="hidden" method="get">
        <input type="hidden" name="username" value="<?= $username ?>" />
        <button type="submit" >Login as <span id="visibleUsername"><?= $username ?></span></button>
        <a id="changeUsername" href="#">Log in as someone else?</a>
    </form>

    <form id="loginForm" action="<?= $returnto ?>" class="hidden" method="get">
        <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" name="username" required pattern="\w+" placeholder="alphanumeric"/>
        </div>
        <button type="submit">Login</button>
    </form>
    <script src="js/lib.js"></script>
    <script src="js/login.js"></script>
</body>
</html>