# Test webpages for [Related Website Sets](https://github.com/WICG/first-party-sets)

This repository contains some mock website code that can be installed on a couple different domains to test Chrome's
Related Website Sets functionality.

For more information see [T359926](https://phabricator.wikimedia.org/T359926) and [T345589](https://phabricator.wikimedia.org/T345589).

# Setting up the sites

You should set up at least two websites on different registrable domains[](https://developer.mozilla.org/en-US/docs/Glossary/Site).
For each, you need to go through these steps:

* Clone the repository
* Copy `config.dist.php` to `config.php` and replace the domain names. You should have a primary site and a login site.
  You can add more domains under `associated` or leave that empty.
* Ensure that the `<domain>/.well-known/related-website-set.json` URL is served by `well-known.php`.
* Set the repo root as the web root.

On Toolforge, the last two steps can be achieved by adding a symlink from `.lighttpd.conf` to `/path/to/repo/lighttpd.conf`,
another symlink from `public_html` to the repository root, and running `webservice --backend=kubernetes php8.2 start`.

# Demo

Try it out under these domains:

* https://sul3-rws-test.toolforge.org/
* https://sul3-rws-login.toolforge.org/

(Note that toolforge.og is on the public suffix list, so these are indeed separate registrable domains.)

To test, set the following feature flags:
* `chrome://flags#test-third-party-cookie-phaseout` to `Enabled`
* optionally, `chrome://flags/#tpcd-heuristics-grants` to `Disabled` ([more info](https://github.com/amaliev/3pcd-exemption-heuristics/blob/main/explainer.md))

(TODO: what's the difference between `test-third-party-cookie-phaseout` and `tracking-protection-3pcd`?)

and start Chrome with
```shell
--use-related-website-set="`curl -s https://sul3-rws-test.toolforge.org/.well-known/related-website-set.json`"
```
(You can verify that it worked at `chrome://system/`.)

# TODO

* how do we handle logout (which needs to be more robust)? Do browsers exempt cookie unset?