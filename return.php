<?php
// Main site entry point for end of redirect chain inside the login popup/iframe.
// In a real application this would be a PHP entry point that sets local cookies.

$config = require_once 'config.php';
require_once 'src/lib.php';

render( 'return', [
    'username' => $_GET['username'] ?? null,
] );