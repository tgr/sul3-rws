// This script is loaded on the embedded login page.
// Depends on lib.js.

const queryParams = new URLSearchParams( location.search );

async function main() {
    const method = queryParams.get( 'method' ), // popup, iframe or invisible iframe login?
        returnto = queryParams.get( 'returnto' ),
        askForPermission = Boolean( queryParams.get( 'askForPermission' ) );

    const accessStatus = await canAccessSite( null, askForPermission );
    if ( accessStatus === StorageAccessStatus.GRANTED
        || ( accessStatus === StorageAccessStatus.PROMPT && askForPermission )
    ) {
        await requestAccessToSite( null );
    }

    let username = getCookie( 'username' );

    if ( method === EmbeddingMethod.INVISIBLE_IFRAME ) {
        const { username: serverSideUsername } = JSON.parse( document.getElementById( 'data' ).innerHTML );
        console.log( username ? 'read username cookie: ' + username : 'could not read username cookie' );
        console.log( serverSideUsername
            ? 'username cookie read on the server side: ' + serverSideUsername
            : 'could not read username cookie on the server side' );

        const returnUrl = new URL( returnto );
        if ( username ) {
            returnUrl.searchParams.set( 'username', username );
        }
        window.location = returnUrl;
    } else if ( username ) {
        console.log( 'read username cookie: ' + username );
        showLoginButton( username );
    } else {
        console.log( 'could not read username cookie' );
        showLoginForm();
    }
}

function getCookie( name ) {
    return document.cookie.split( ';' )
            .find( ( row ) => row.startsWith( name + '=' ) )
            ?.split("=")[1]
        || null;
}

function showLoginButton( username ) {
    const loginButtonForm = document.getElementById( 'loginButtonForm' );

    let serverSideUsername = loginButtonForm.elements['username'].value;
    if ( serverSideUsername ) {
        console.log( 'username cookie read on the server side: ' + serverSideUsername );
    }

    loginButtonForm.elements['username'].value = username;
    document.getElementById( 'visibleUsername' ).innerText = username;

    document.getElementById( 'changeUsername' ).addEventListener( 'click', ( event ) => {
        showLoginForm();
    } );

    loginButtonForm.classList.remove( 'hidden' );
    document.getElementById( 'loginForm' ).classList.add( 'hidden' );
}

function showLoginForm() {
    const loginForm = document.getElementById( 'loginForm' );
    loginForm.addEventListener( 'submit', ( event ) => {
        const username = loginForm.elements['username'].value;
        // Chrome requires the cookie to have SameSite=None and Secure to allow unpartitioning via requestStorageAccess.
        document.cookie = `username=${username};SameSite=None;Secure`;
    } );

    loginForm.classList.remove( 'hidden' );
    document.getElementById( 'loginButtonForm' ).classList.add( 'hidden' );
}

main();