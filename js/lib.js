// See:
// https://developer.mozilla.org/en-US/docs/Web/API/Storage_Access_API
// https://developer.mozilla.org/en-US/docs/Web/Privacy/Storage_Access_Policy
// https://developers.google.com/privacy-sandbox/3pcd/storage-access-api
// https://developers.google.com/privacy-sandbox/3pcd/related-website-sets-integration

StorageAccessStatus = {
    /** The script can access unpartitioned cookies on the target site. */
    GRANTED: 'granted',
    /** The script cannot access unpartitioned cookies on the target site. */
    DENIED: 'denied',
    /** The script cannot access unpartitioned cookies on the target site, but we can show a permission prompt. */
    PROMPT: 'prompt',
};

AuthAction = {
    /** Show a login form (or button, if the user is already logged in centrally) if we have to. */
    LOGIN: 'login',
    /** Try to identify the user without interaction, but don't do anything if that fails. */
    AUTOLOGIN: 'autologin',
};

EmbeddingMethod = {
    FETCH: 'fetch',
    POPUP: 'popup',
    IFRAME: 'iframe',
    INVISIBLE_IFRAME: 'invisible-iframe',
    TOPLEVEL: 'toplevel',
};

/**
 * Check whether we can access unpartitioned cookies in the current context.
 * When the 'site' parameter is provided, this will check whether that site will be able to access unpartitioned
 * cookies when embedded in the current site. The function must be called from the top-level document due to browser
 * security restrictions.
 * When the 'site' parameter is not provided, this will check whether the current site can access unpartitioned
 * cookies when embedded in its current parent site. (If the current document is not embedded, there is no partitioning
 * so calling this function is pointless.)
 *
 * Note that in Chrome, access is limited to certain types of cookies:
 * https://developer.mozilla.org/en-US/docs/Web/API/Storage_Access_API#browser-specific_variations
 *
 * @param {?string} site Site to check (e.g. 'https://en.wikipedia.org'). If null, the current site is used, with
 *   requestStorageAccess; otherwise the given site, with requestStorageAccessFor.
 * @param {boolean} askForPermission Assume we are willing to trigger the browser's permission popup if needed.
 *   (Calling this function will never trigger it.)
 * @return {Promise<string>} A StorageAccessStatus constant. GRANTED means we have access; DENIED means we can't
 *   obtain it; PROMPT means requestAccessToSite() can be called, and it will result in a browser-specific
 *   permission prompt.
 */
async function canAccessSite( site, askForPermission ) {
    if ( !document.requestStorageAccess ) {
        // Old browser, does not block third-party cookies.
        return StorageAccessStatus.GRANTED;
    } else if ( site && !document.requestStorageAccessFor ) {
        // Modern browser but does not support requestStorageAccessFor, so there is no way to check if we can access
        // cookies on a site other than the current one.
        return StorageAccessStatus.DENIED;
    }

    // document.hasStorageAccess() is unreliable in both directions per
    // https://developer.mozilla.org/en-US/docs/Web/API/Document/hasStorageAccess#return_value
    // so we check it but don't use it.
    const hasAccess = document.hasStorageAccess();
    console.log( 'document.hasStorageAccess(): ' + ( hasAccess ? 'true' : 'false' ) );

    // These are just for logging.
    const methodName = site ? 'requestStorageAccessFor' : 'requestStorageAccess';
    const permissionName = site ? 'top-level-storage-access' : 'storage-access';

    try {
        const status = site
            ? await navigator.permissions.query( { name: 'top-level-storage-access', requestedOrigin: site } )
            : await navigator.permissions.query( { name: 'storage-access' } );
        if ( status.state === 'granted' ) {
            // This can mean several things:
            // - Permission was already granted by the user within the last 30 days.
            // - The user has configured the browser to not block third-party cookies.
            // - The browser is Chrome and has an RWS rule allowing third-party cookie access.
            // - Access is allowed by some browser-specific heuristic. E.g. Firefox auto-grants a small fixed number
            //   of access requests per domain; Firefox and Chrome exempt some OAuth-like interaction patterns:
            //   - https://developer.mozilla.org/en-US/docs/Web/Privacy/Storage_Access_Policy#automatic_storage_access_upon_interaction
            //   - https://developers.google.com/privacy-sandbox/3pcd/temporary-exceptions/heuristics-based-exceptions
            // - This is not a situation where partitioning happens at all (e.g. the function is called without a
            //   'site' parameter, and the current document is the top-level document).
            // We still need to call document.requestStorageAccess[For] to access cookies in this request, but it does
            // not require user interaction and will be auto-granted. (We handle rejections just in case, but they
            // should never happen here.)
            // FIXME it seems for at least some of the cases that don't involve a manual granting from the user,
            //  the permission query will return 'prompt' and then auto-grant without a prompt anyway:
            //  https://privacycg.github.io/requestStorageAccessFor/#:~:text=Note%20that%20when,permissionState%20is%20granted%3A
            const request = site
                ? document.requestStorageAccessFor( site )
                : document.requestStorageAccess();
            return await request.then(
                () => {
                    console.log( `navigator.permissions.query( "${permissionName}" ): granted` );
                    return StorageAccessStatus.GRANTED;
                },
                ( error ) => {
                    console.log( `navigator.permissions.query( "${permissionName}" ): granted but ${methodName} failed: `, error );
                    return StorageAccessStatus.DENIED;
                }
            );
        } else if ( status.state === 'prompt'  ) {
            // This can mean several things:
            // - The user has not granted permission yet (or the grant expired). We can ask via a permission popup.
            // - The user has denied permission in the past (but for privacy reasons the browser pretends they didn't).
            //   The request will be auto-denied.
            // - The user has configured the browser to not even show permission popups. As above, will be auto-denied.
            // - The user has not interacted with the target site ('site' parameter or current site) in the last
            //   30 days in a non-embedded context. As above, will be auto-denied.
            //   (Per https://developer.mozilla.org/en-US/docs/Web/API/Storage_Access_API#security_measures #3.
            //   TODO Is "in a non-embedded context" correct? It doesn't really make sense.)
            // - With the 'site' parameter, the permission query will always return 'prompt' and then auto-deny unless
            //   the two sites are in the same RWS set.
            if ( askForPermission ) {
                console.log( `navigator.permissions.query( "${permissionName}" ): prompt` );
                return StorageAccessStatus.PROMPT;
            } else {
                console.log( `navigator.permissions.query( "${permissionName}" ): prompt but we are not asking` );
                return StorageAccessStatus.DENIED;
            }
        } else /* status.state === 'denied' */ {
            // Not actually used per https://github.com/privacycg/storage-access/issues/149 but handle it just in case.
            console.log( 'navigator.permissions.query( "storage-access" ): denied' );
            return false;
        }
    } catch ( e ) {
        // This can happen for a number of reasons, see
        // https://developer.mozilla.org/en-US/docs/Web/API/Permissions/query#exceptions
        // https://developer.mozilla.org/en-US/docs/Web/API/Document/requestStorageAccess#exceptions
        console.error( e );
        return false;
    }
}

/**
 * Try to get access unpartitioned cookies in the current context.
 * This should be called when canAccessSite() returned PROMPT, and should always be called after user interaction.
 *
 * @param {?string} site Site to check (e.g. 'https://en.wikipedia.org'). If null, the current site is used, with
 *   requestStorageAccess; otherwise the given site, with requestStorageAccessFor.
 * @return {Promise<boolean>} Whether access was granted.
 */
async function requestAccessToSite( site ) {
    if ( navigator.userActivation && !navigator.userActivation.isActive ) {
        console.log( 'requestAccessToSite() called but navigator.userActivation.isActive is false' );
    }
    const methodName = site ? 'requestStorageAccessFor' : 'requestStorageAccess';
    const request = site
        ? document.requestStorageAccessFor( site )
        : document.requestStorageAccess();
    return await request.then(
        () => {
            console.log( `${methodName} granted` );
            return true;
        },
        ( error ) => {
            console.log( `${methodName} denied: `, error );
            return false;
        }
    );
}