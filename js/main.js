// This script is loaded on the local (embedding) site. If requestStorageAccessFor is available, it uses that;
// otherwise requesting permission is left to the embedded page.
// Depends on lib.js.

const action = document.getElementById( 'action' ).value, // login or autologin?
    useRequestStorageAccessFor = Boolean( document.getElementById( 'useRequestStorageAccessFor' ).value ),
    askForPermission = Boolean( document.getElementById( 'askForPermission' ).value )
        // Don't trigger permission popups for autologin.
        && action === AuthAction.LOGIN;

const { loginSite, returnto } = JSON.parse( document.getElementById( 'data' ).innerHTML );

let username = false;

function setupEventHandlers() {
    document.getElementById( 'loginLink' ).addEventListener( 'click', async ( event ) => {
        event.preventDefault();
        // sanity check
        if ( !navigator.userActivation.isActive ) {
            console.log( 'navigator.userActivation.isActive is false' );
        }

        const method = document.getElementById( 'method' ).value; // popup or iframe login?
        if (method === EmbeddingMethod.POPUP) {
            username = await authenticateViaPopup();
        } else /* method === EmbeddingMethod.IFRAME */ {
            username = await authenticateViaIframe();
        }
        document.getElementById( 'result' ).textContent = username || 'Failed to log in';
    } );
    document.getElementById( 'permissionButton' ).addEventListener( 'click',async ( event ) => {
        event.preventDefault();
        // sanity check
        if ( !navigator.userActivation.isActive ) {
            console.log( 'navigator.userActivation.isActive is false' );
        }

        username = await requestAccessAndAuthenticateViaFetch( loginSite );
        document.getElementById( 'result' ).textContent = username || 'Failed to autologin, please log in manually';
    } );
}

async function main() {
    let needsTopLevelStorageAccess = false;

    if ( useRequestStorageAccessFor ) {
        // Try to get access via requestStorageAccessFor + RWS, and check central auth cookies via AJAX.
        const accessStatus = await canAccessSite( loginSite, askForPermission );
        if ( accessStatus === StorageAccessStatus.GRANTED ) {
            username = await authenticateViaFetch();
        } else if ( accessStatus === StorageAccessStatus.PROMPT ) {
            needsTopLevelStorageAccess = true;
        }
    }

    if ( !username ) {
        username = await authenticateViaInvisibleIframe();
    }

    if ( username ) {
        document.getElementById('result' ).textContent = username;
    } else {
        if ( needsTopLevelStorageAccess ) {
            document.getElementById( 'permissionButtonContainer' ).classList.remove( 'hidden' );
        }

        if ( action === AuthAction.LOGIN ) {
            // We did not have permission to check central auth cookies via AJAX. Or maybe we did, but the cookies were
            // not set. It doesn't seem like we can distinguish these cases.
            // Try requestStorageAccess with a prompt, which must be preceded by user interaction.
            let loginLink = document.getElementById( 'loginLink' ),
                loginLinkTooltip = document.getElementById( 'loginLinkTooltip' );
            loginLinkTooltip.style.top = loginLink.offsetTop + loginLink.offsetHeight + 'px';
            loginLinkTooltip.style.left = loginLink.offsetLeft + 'px';
            loginLinkTooltip.classList.remove( 'hidden' );
            loginLinkTooltip.classList.add( 'flash' );
        }
    }
}

/**
 * Prompt  the user for storage access, then call authenticateViaFetch().
 * Must be called after a user gesture.
 * @return {Promise<string|boolean>} The username, or false if the user could not be identified.
 */
async function requestAccessAndAuthenticateViaFetch() {
    // TODO Is there any benefit to this, given that it requires a gesture?
    // FIXME https://privacycg.github.io/requestStorageAccessFor/#security says
    //  "frame-level access is only granted once requestStorageAccess() is successfully invoked.
    //   For frame access, requestStorageAccessFor(requestedOrigin) merely simplifies the activation
    //   and prompting requirements."
    //  What does that mean?
    return requestAccessToSite( loginSite ).then(
        () => authenticateViaFetch(),
        () => false
    );
}

/**
 * Check central auth cookies on the server side via AJAX.
 * Used with requestStorageAccessFor and on old browsers.
 * @return {Promise<string|boolean>} The username, or false if the user could not be identified.
 */
async function authenticateViaFetch() {
    const url = new URL( loginSite + '/login.php' );
    url.searchParams.set( 'method', 'fetch' );

    try {
        const response = await fetch( url, {
            credentials: 'include',
            cache: 'no-cache',
        } );
        const json = await response.json();
        const username = json.username || false;
        console.log( 'Authenticated via fetch: ' + ( username || 'not logged in' ) );
        return username;
    } catch ( e ) {
        console.log( 'Failed to authenticate via fetch' );
        return false;
    }
}

/**
 * Check central auth cookies via an invisible iframe.
 * Used as an opportunistic autologin mechanism.
 * @return {Promise<boolean>}
 */
async function authenticateViaInvisibleIframe() {
    const url = new URL( loginSite + '/login.php' );
    url.searchParams.set( 'method', 'invisible-iframe' );
    url.searchParams.set( 'askForPermission', askForPermission ? '1' : '' );
    url.searchParams.set( 'returnto', returnto );

    const iframe = document.createElement( 'iframe' );
    iframe.src = url;
    iframe.width = 0;
    iframe.height = 0;
    document.querySelector( 'body' ).appendChild( iframe );

    try {
        await detectAuthenticationEnd( iframe.contentWindow, 5000 );
        const url = new URL( iframe.contentWindow.location.href );
        document.querySelector( 'body' ).removeChild( iframe );
        const username = url.searchParams.get( 'username' ) || false;
        console.log( 'Authenticated via invisible iframe: ' + ( username || 'not logged in' ) );
        return username;
    } catch ( e ) {
        console.log( 'Failed to authenticate via invisible iframe: ', e );
        return false;
    }
}

/**
 * Show a login form (or button if the user is already logged in centrally) in a popup.
 * @return {Promise<string|boolean>} The username, or false if the user could not be identified.
 */
async function authenticateViaPopup() {
    const url = new URL( loginSite + '/login.php' );
    url.searchParams.set( 'method', 'popup' );
    url.searchParams.set( 'askForPermission', askForPermission ? '1' : '' );
    url.searchParams.set( 'returnto', returnto );

    const handle = open( url, 'centralauthpopup', 'popup,width=600,height=400' );
    if ( !handle ) {
        // Caught by popup blocker?
        console.log( 'Failed to open popup' );
        return false;
    }

    try {
        await detectAuthenticationEnd( handle );
        const url = new URL( handle.location.href );
        handle.close();
        const username = url.searchParams.get( 'username' ) || false;
        console.log( 'Authenticated via popup: ' + ( username || 'not logged in' ) );
        return username;
    } catch ( e ) {
        console.log( 'Failed to authenticate via popup: ', e );
        return false;
    }
}

/**
 * Same as authenticateViaPopup, but in an iframe.
 * This is more robust as it is not subject to popup blockers.
 * @return {Promise<boolean>}
 */
async function authenticateViaIframe() {
    const url = new URL( loginSite + '/login.php' );
    url.searchParams.set( 'method', 'iframe' );
    url.searchParams.set( 'askForPermission', askForPermission ? '1' : '' );
    url.searchParams.set( 'returnto', returnto );

    const iframe = document.createElement( 'iframe' );
    iframe.src = url;
    iframe.width = 600
    iframe.height = 400;
    document.getElementById( 'iframeContainer' ).appendChild( iframe );

    try {
        await detectAuthenticationEnd( iframe.contentWindow );
        const url = new URL( iframe.contentWindow.location.href );
        document.getElementById( 'iframeContainer' ).removeChild( iframe );
        const username = url.searchParams.get( 'username' ) || false;
        console.log( 'Authenticated via iframe: ' + ( username || 'not logged in' ) );
        return username;
    } catch ( e ) {
        console.log( 'Failed to authenticate via iframe: ', e );
        return false;
    }
}

/**
 * Detect when authentication in a cross-site popup window or iframe finishes.
 * @param {WindowProxy} windowProxy A popup or iframe reference holding a cross-site page. It is expected to redirect
 *   back to a same-origin page after authentication ends. The URL of the final redirect must not be identical to
 *   the URL of the current document.
 * @param {?int} timeout Timeout in milliseconds.
 * @return {Promise<void>} Will reject if authentication was aborted (e.g. the user closed the popup).
 */
async function detectAuthenticationEnd( windowProxy, timeout ) {
    // Hacky timer-based solution. A real application would probably use postMessage, or maybe window.name if it
    // wants to support older browsers.
    return Promise.race( [
        new Promise( ( resolve, reject ) => {
            const poll = function () {
                if ( windowProxy.closed ) {
                    // The user closed the popup without finishing authentication.
                    reject();
                }
                try {
                    if ( windowProxy.location.href === location.href ) {
                        // This is the initial state of the popup, before it loads.
                        setTimeout( poll, 100 );
                        return;
                    } else if ( windowProxy.location.origin === 'null' ) {
                        // This is the initial state of the iframe, before it loads.
                        setTimeout( poll, 100 );
                        return;
                    }
                    // If we got here, authentication has finished.
                    resolve();
                } catch ( e ) {
                    // The popup is still on a different origin, so we can't access its location.
                    setTimeout( poll, 100 );
                }
            };
            setTimeout( poll, 100 );
        } ),
        new Promise( ( resolve, reject ) => {
            if ( timeout ) {
                setTimeout( reject, timeout, new Error( `detectAuthenticationEnd aborted after ${timeout} ms` ) );
            }
        } )
    ] );
}

setupEventHandlers();
main();