<?php
// Login site entry point for rendering a login popup/iframe.

$config = require_once 'config.php';
require_once 'src/lib.php';

assertCurrentSite( $config['sites']['login'] );

// TODO this is needed for fetch. Is it needed for the others?
//  https://privacycg.github.io/requestStorageAccessFor/#subresources seems to say yes:
//  "For any SameSite=None cookies to be included, the request’s mode has to be "cors",
//   where reading of the response is blocked unless the embeddee opts-in via sending
//   the appropriate `access-control-allow-credentials` header."
$origin = $_SERVER['HTTP_ORIGIN'] ?? null;
if ( $origin /* a real application would check against the list of known domains here */ ) {
    header( 'Access-Control-Allow-Origin: ' . $origin );
    header( 'Access-Control-Allow-Credentials: true' );
}

$method = $_GET['method'];

if ( $method === 'fetch' ) {
    // Used for requestStorageAccessFor.
    // Per https://privacycg.github.io/requestStorageAccessFor/#subresources
    // and https://privacycg.github.io/requestStorageAccessFor/#fetch-integration #1 / #2
    // "only requests initiated from the top-level document will be eligible for
    //  inclusion of SameSite=None cookies. This ensures that other embedded
    //  frames do not receive escalated privileges."
    // So this login method won't work on a wiki page that's embedded into some other application.
    // On the net, probably a good thing?
    echo json_encode( [
        'username' => $_COOKIE['username'] ?? null,
    ] );
} elseif ( $method === 'invisible-iframe' ) {
    header( 'Content-Security-Policy: frame-ancestors ' . implode( ' ', getCSPDomainList( $config ) ) );
    render( 'invisibleIframe', [
        'username' => $_COOKIE['username'] ?? null,
        'returnto' => $_GET['returnto'],
    ] );
} else {
    // popup or iframe
    if ( $method === 'iframe' ) {
        header( 'Content-Security-Policy: frame-ancestors ' . implode( ' ', getCSPDomainList( $config ) ) );
    }
    render( 'loginForm', [
        'username' => $_COOKIE['username'] ?? null,
        'returnto' => $_GET['returnto'],
    ] );
}