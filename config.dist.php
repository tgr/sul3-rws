<?php

return [
    'sites' => [
        'primary' => 'https://wikimediafoundation.org',
        'login' => 'https://login.wikimedia.org',
        'associated' => [
            'https://mediawiki.org',
            'https://wikibooks.org',
            'https://wikidata.org',
            'https://wikimedia.org',
            'https://wikinews.org',
            'https://wikiquote.org',
            'https://wikipedia.org',
            'https://wikisource.org',
            'https://wikiversity.org',
            'https://wikivoyage.org',
            'https://wiktionary.org',
            'https://wikifunctions.org',
        ],
    ],
];